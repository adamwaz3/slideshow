import { Component } from "react";
import { Query } from "react-apollo";
import {
  Card,
  Frame,
  Heading,
  Loading,
  Page,
  ResourceList,
  Stack,
  TextContainer,
  TextStyle,
  Thumbnail,
} from "@shopify/polaris";
import { GET_PRODUCTS } from "../server/handlers/queries/products";

export default class Index extends Component {
  state = {
    selectedProducts: [],
    sortValue: "VISIBLE_DESC",
    searchValue: "",
    visibleProducts: [],
  };
  products;

  componentDidMount() {
    this.fetchVisibleProducts();
  }

  handleSortChange = (items, sortValue) => {
    this.setState({ sortValue });
    this.sortProducts(items, sortValue);
  };

  handleSelectionChange = (selectedProducts) => {
    this.setState({ selectedProducts });
  };

  handleItemSelectionChange = (selectedProduct) => {
    const selectedProducts = this.state.selectedProducts;
    if (!selectedProducts.length) {
      this.setState({ selectedProducts: [selectedProduct] });
    }
  };

  changeSelectedProductsVisibility(visible) {
    const visibleProducts = this.state.visibleProducts;

    this.state.selectedProducts.forEach((selectedProduct) => {
      const product = this.products.find(
        (product) => product.id === selectedProduct
      );
      const visibleProductIndex = visibleProducts.findIndex(
        (product) => product.id === selectedProduct
      );

      if (!product.image || (visibleProductIndex !== -1) === visible) {
        return;
      }

      if (visible) {
        visibleProducts.push(product);
      } else {
        visibleProducts.splice(visibleProductIndex, 1);
      }
    });

    this.setState({ visibleProducts });
    this.pushProductsToServer();
    this.handleSelectionChange([]);
  }

  sortProducts = (products) => {
    const visibleProductsMap = {};
    this.state.visibleProducts.forEach((visibleProduct) => {
      visibleProductsMap[visibleProduct.id] = true;
    });

    products.sort((a, b) => {
      if (visibleProductsMap[a.id] === visibleProductsMap[b.id]) {
        return 0;
      }
      const sortChange = this.state.sortValue === "VISIBLE_DESC" ? 1 : -1;
      return visibleProductsMap[a.id] ? -1 * sortChange : sortChange;
    });

    return products;
  };

  pushProductsToServer = () => {
    fetch("/products", {
      method: "PUT",
      body: JSON.stringify(this.state.visibleProducts),
      headers: { "Content-Type": "application/json" },
    });
  };

  fetchVisibleProducts = () => {
    fetch("/products")
      .then((response) => response.json())
      .then((products) => this.setState({ visibleProducts: products }));
  };

  renderProduct = (product) => {
    const { id, title, image } = product;
    const isVisible = this.state.visibleProducts.some(
      (visibleProduct) => visibleProduct.id === product.id
    );
    const media = (
      <Thumbnail
        source={image ? image.originalSrc : ""}
        alt={image ? image.altText : ""}
      />
    );

    return (
      <ResourceList.Item
        verticalAlignment="center"
        id={id}
        media={media}
        onClick={this.handleItemSelectionChange}
      >
        <Stack>
          <Stack.Item fill>
            <h3>
              <TextStyle variation="strong">{title}</TextStyle>
            </h3>
          </Stack.Item>
          <Stack.Item>{isVisible ? "Visible" : "Hidden"}</Stack.Item>
        </Stack>
      </ResourceList.Item>
    );
  };

  renderList = (products) => {
    const resourceName = {
      singular: "product",
      plural: "products",
    };

    const promotedBulkActions = [
      {
        content: "Show in slideshow",
        onAction: () => this.changeSelectedProductsVisibility(true),
      },
      {
        content: "Hide in slideshow",
        onAction: () => this.changeSelectedProductsVisibility(false),
      },
    ];

    const sortOptions = [
      { label: "Visible", value: "VISIBLE_DESC" },
      { label: "Hidden", value: "VISIBLE_ASC" },
    ];

    return (
      <ResourceList
        resourceName={resourceName}
        items={products}
        renderItem={this.renderProduct}
        selectedItems={this.state.selectedProducts}
        onSelectionChange={this.handleSelectionChange}
        promotedBulkActions={promotedBulkActions}
        sortValue={this.state.sortValue}
        sortOptions={sortOptions}
        onSortChange={(sortValue) => this.handleSortChange(products, sortValue)}
      />
    );
  };

  renderQuery = () => {
    return (
      <Query query={GET_PRODUCTS}>
        {({ data, loading, error }) => {
          if (loading) {
            return (
              <Frame>
                <Loading />
              </Frame>
            );
          }

          if (error) {
            return (
              <TextContainer>
                <Heading>Problem during products load</Heading>
                <p>Try again or contact app creator.</p>
              </TextContainer>
            );
          }

          this.products = this.transformResponseToProducts(data.products);
          this.products = this.sortProducts(this.products);
          return this.renderList(this.products);
        }}
      </Query>
    );
  };

  render() {
    return (
      <Page>
        <Card>{this.renderQuery()}</Card>
      </Page>
    );
  }

  transformResponseToProducts(items) {
    return items.edges.map((item) => {
      const {
        id,
        legacyResourceId,
        title,
        onlineStoreUrl,
        onlineStorePreviewUrl,
        images,
      } = item.node;
      const image = images.edges[0] ? images.edges[0].node : {};
      const { originalSrc, altText } = image;

      return {
        id,
        legacyResourceId,
        title,
        url: onlineStoreUrl || onlineStorePreviewUrl,
        image: originalSrc ? { originalSrc, altText } : null,
      };
    });
  }
}
