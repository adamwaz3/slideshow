import store from "store-js";

export function webhooksProductsUpdate(ctx) {
  const webhookData = ctx.state.webhook;
  const products = store.get(webhookData.domain, []);
  const product = products.find((p) => p.id === webhookData.payload.admin_graphql_api_id);

  if (product) {
    const { src, alt } = webhookData.payload.image;

    product.title = webhookData.payload.title;
    product.image = {
      originalSrc: src,
      altText: alt,
    };

    store.set(webhookData.domain, products);
  }
}
