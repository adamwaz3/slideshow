import store from "store-js";

export function webhooksProductsDelete(ctx) {
  const webhookData = ctx.state.webhook;
  const products = store.get(webhookData.domain, []);
  const index = products.findIndex((p) => p.legacyResourceId === webhookData.payload.id.toString());

  if (index !== -1) {
    products.splice(index, 1);
    store.set(webhookData.domain, products);
  }
}
