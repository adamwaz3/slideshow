import store from "store-js";

export function webhooksAppUninstalled(ctx) {
  store.remove(ctx.state.webhook.domain);
}
