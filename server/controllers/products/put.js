import store from "store-js";

export function productsPut(ctx) {
  ctx.body = store.set(ctx.session.shop, ctx.request.body);
  ctx.res.statusCode = 200;
}
