import store from "store-js";

export function productsGet(ctx) {
  const shop = ctx.session.shop || ctx.query.shop;
  ctx.body = store.get(shop, []);
  ctx.res.statusCode = 200;
}
