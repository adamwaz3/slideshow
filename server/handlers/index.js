import { createClient } from "./client";
import { scriptTagCreate } from "./mutations/script-tag-create";
import { registerWebhooks } from "./register-webhooks";

export { createClient, scriptTagCreate, registerWebhooks };
