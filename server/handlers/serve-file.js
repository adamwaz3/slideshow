import fs from "fs";

export function serveFile(path) {
  return (ctx) => {
    fs.readFile(path, (err, data) => {
      if (err) {
        console.log(err);
      }
      ctx.res.writeHead(200);
      ctx.res.end(data);
    });
    ctx.respond = false;
  };
}
