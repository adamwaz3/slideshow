import gql from "graphql-tag";

export const GET_PRODUCTS = gql`
  query {
    products(first: 100) {
      edges {
        node {
          id
          legacyResourceId
          title
          onlineStoreUrl
          onlineStorePreviewUrl
          images(first: 1) {
            edges {
              node {
                originalSrc
                altText
              }
            }
          }
        }
      }
    }
  }
`;
