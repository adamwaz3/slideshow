import "isomorphic-fetch";
import { gql } from "apollo-boost";

export function SCRIPT_TAG_CREATE() {
  return gql`
    mutation scriptTagCreate($input: ScriptTagInput!) {
      scriptTagCreate(input: $input) {
        scriptTag {
          id
        }
        userErrors {
          field
          message
        }
      }
    }
  `;
}

export const scriptTagCreate = async (ctx) => {
  const { client } = ctx;
  return await client
    .mutate({
      mutation: SCRIPT_TAG_CREATE(),
      variables: {
        input: {
          src: process.env.HOST + "/slideshow.js",
        },
      },
    })
    .then((response) => {
      console.log('Successfully created ScriptTag');
      return response.data.scriptTagCreate.scriptTag
    });
};
