import createShopifyAuth from "@shopify/koa-shopify-auth";
import * as handlers from "./index";
import { ApiVersion } from "@shopify/koa-shopify-graphql-proxy";

const { SHOPIFY_API_SECRET, SHOPIFY_API_KEY, SCOPES } = process.env;

export function shopifyAuth(server) {
  return createShopifyAuth({
    apiKey: SHOPIFY_API_KEY,
    secret: SHOPIFY_API_SECRET,
    scopes: [SCOPES],

    async afterAuth(ctx) {
      //Auth token and shop available in session
      //Redirect to shop upon auth
      const { shop, accessToken } = ctx.session;
      await handlers.registerWebhooks(
        shop,
        accessToken,
        "APP_UNINSTALLED",
        "/webhooks/app/uninstalled",
        ApiVersion.October19
      );
      await handlers.registerWebhooks(
        shop,
        accessToken,
        "PRODUCTS_DELETE",
        "/webhooks/products/delete",
        ApiVersion.October19
      );
      await handlers.registerWebhooks(
        shop,
        accessToken,
        "PRODUCTS_UPDATE",
        "/webhooks/products/update",
        ApiVersion.October19
      );
      ctx.cookies.set("shopOrigin", shop, {
        httpOnly: false,
        secure: true,
        sameSite: "none",
      });
      server.context.client = await handlers.createClient(shop, accessToken);
      await handlers.scriptTagCreate(ctx);
      ctx.redirect("/");
    },
  });
}
