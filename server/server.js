import "@babel/polyfill";
import dotenv from "dotenv";
import "isomorphic-fetch";
import { verifyRequest } from "@shopify/koa-shopify-auth";
import graphQLProxy, { ApiVersion } from "@shopify/koa-shopify-graphql-proxy";
import Koa from "koa";
import koaBody from "koa-body";
import next from "next";
import Router from "koa-router";
import session from "koa-session";
import { receiveWebhook } from "@shopify/koa-shopify-webhooks";
import { webhooksProductsUpdate } from "./controllers/webhooks/products/update";
import { webhooksProductsDelete } from "./controllers/webhooks/products/delete";
import { webhooksAppUninstalled } from "./controllers/webhooks/app/uninstalled";
import { shopifyAuth } from "./handlers/shopify-auth";
import { serveFile } from "./handlers/serve-file";
import { productsGet } from "./controllers/products/get";
import { productsPut } from "./controllers/products/put";

dotenv.config();
const port = parseInt(process.env.PORT, 10) || 8081;
const dev = process.env.NODE_ENV !== "production";
const app = next({
  dev,
});
const handle = app.getRequestHandler();
const { SHOPIFY_API_SECRET } = process.env;

app.prepare().then(() => {
  const server = new Koa();
  const router = new Router();
  const webhook = receiveWebhook({
    secret: SHOPIFY_API_SECRET,
  });
  server.use(
    session(
      {
        sameSite: "none",
        secure: true,
      },
      server
    )
  );
  server.keys = [SHOPIFY_API_SECRET];
  server.use(shopifyAuth(server));
  server.use(
    graphQLProxy({
      version: ApiVersion.October19,
    })
  );
  router.post("/webhooks/products/update", webhook, webhooksProductsUpdate);
  router.post("/webhooks/products/delete", webhook, webhooksProductsDelete);
  router.post("/webhooks/app/uninstalled", webhook, webhooksAppUninstalled);
  router.get("/products", productsGet);
  router.put("/products", verifyRequest(), koaBody(), productsPut);
  router.get(
    "/slideshow.js",
    verifyRequest(),
    serveFile(__dirname + "/../slideshow/dist/slideshow.js")
  );
  router.get("*", verifyRequest(), async (ctx) => {
    await handle(ctx.req, ctx.res);
    ctx.respond = false;
    ctx.res.statusCode = 200;
  });
  server.use(router.allowedMethods());
  server.use(router.routes());
  server.listen(port, () => {
    console.log(`> Ready on http://localhost : ${port}`);
  });
});
