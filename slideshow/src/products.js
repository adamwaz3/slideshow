export class Products {
  static getProducts() {
    return fetch("/apps/slideshow/products", {
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .catch((E) => console.log(E));
  }
}
