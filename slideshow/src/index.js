import { Slideshow } from "./slideshow";

const slideshowPlaceholders = document.getElementsByClassName(
  "toptal-product-slider"
);
for (const placeholder of slideshowPlaceholders) {
  new Slideshow(placeholder);
}
