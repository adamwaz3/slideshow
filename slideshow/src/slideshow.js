import css from "./slideshow.css";
import { Products } from "./products";

export class Slideshow {
  placeholderElement = null;
  productsElement = null;
  leftArrowElement = null;
  rightArrowElement = null;
  productElements = [];
  firstElementIndex = 0;

  constructor(placeholderElement) {
    console.log(placeholderElement);
    this.placeholderElement = placeholderElement;
    this.build();
  }

  get elementsCountInViewport() {
    switch (true) {
      case window.matchMedia("(min-width: 1024px)").matches:
        return 3;
      case window.matchMedia("(min-width: 768px)").matches:
        return 2;
      default:
        return 1;
    }
  }

  get maxFirstElementIndex() {
    return this.productElements.length - this.elementsCountInViewport;
  }

  build() {
    this.buildStructure();
    this.addArrowEvents();
    this.addProductElements();
    this.handleResize();
  }

  buildStructure() {
    this.placeholderElement.innerHTML = `
      <div class="slideshow">
        <button class="arrow arrow--left"></button>
        <div class="products"></div>
        <button class="arrow arrow--right"></button>
      </div>
    `;

    this.productsElement = document.getElementsByClassName("products")[0];
  }

  addArrowEvents() {
    const leftArrow = document.getElementsByClassName("arrow--left")[0];
    const rightArrow = document.getElementsByClassName("arrow--right")[0];

    leftArrow.addEventListener("click", () => {
      this.setFirstElementIndex(this.firstElementIndex - 1);
    });
    rightArrow.addEventListener("click", () => {
      this.setFirstElementIndex(this.firstElementIndex + 1);
    });
  }

  addProductElements() {
    Products.getProducts().then((products) => {
      products.forEach((product) => {
        const productElement = this.createProductElement(product);
        this.productsElement.appendChild(productElement);
        this.productElements.push(productElement);
      });
    });
  }

  createProductElement(product) {
    const template = document.createElement("template");
    template.innerHTML = `
      <a class="product" href="${product.url}" target="_blank">
        <div class="product__image"
             style="background-image: url(${product.image.originalSrc})"
             alt="${product.image.altText}">
            <div class="product__name">${product.title}</div>
        </div>
      </div>
    `;
    return template.content.firstElementChild;
  }

  setFirstElementIndex(index) {
    if (index < 0 || index > this.maxFirstElementIndex) {
      return;
    }

    this.firstElementIndex = index;
    this.scrollToFirstElement(true);
  }

  scrollToFirstElement(smooth = false) {
    const productWidth = this.productElements[0]?.scrollWidth;
    this.productsElement.scrollTo({
      top: 0,
      left: this.firstElementIndex * productWidth,
      behavior: smooth ? "smooth" : "auto",
    });
  }

  handleResize() {
    window.addEventListener("resize", () => {
      if (this.firstElementIndex > this.maxFirstElementIndex) {
        this.firstElementIndex = this.maxFirstElementIndex;
      }

      this.scrollToFirstElement();
    });
  }
}
